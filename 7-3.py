# Напишіть функцію-генератор для отримання n перших простих чисел.


def is_prime(num):
    # Checks whether a number is prime
    if num < 2:
        return False
    for i in range(2, int(num**0.5) + 1):
        if num % i == 0:
            return False
    return True


def prime_gen(n):
    # Generator for obtaining the first n prime numbers.
    count = 0
    num = 2
    while count < n:
        if is_prime(num):
            yield num
            count += 1
        num += 1


# The first 10 prime numbers
for prime in prime_gen(10):
    print(prime)
