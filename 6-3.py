# Напишіть ітератор, який повертає елементи заданого списку у зворотному порядку (аналог reversed).


class ReverseIteration:
    def __init__(self, sequence):
        self.sequence = sequence
        self.index = len(sequence)

    # Returns the same object
    def __iter__(self):
        return self

    # Returns the next element of the sequence each time it is called
    def __next__(self):
        if self.index == 0:
            raise StopIteration
        self.index -= 1
        return self.sequence[self.index]


# Applying
my_list = [1, 2, 3, 4, 5]
reverse_iter = ReverseIteration(my_list)
for i in reverse_iter:
    print(i)
