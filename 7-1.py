# Напишіть генератор, який повертає елементи заданого списку у зворотному порядку (аналог reversed).


def reverse_generator(sequence):
    # iterates from the last index of the sequence
    # to the first inclusive with a step of -1.
    for i in range(len(sequence) - 1, -1, -1):
        yield sequence[i]


my_seq = [0, 1, 2, 3, 4, 5]
rev_gen = reverse_generator(my_seq)

for item in rev_gen:
    print(item)
