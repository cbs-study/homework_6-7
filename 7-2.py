# Виведіть із списку чисел список квадратів парних чисел. Використовуйте 2 варіанти рішення: генератор та цикл


numbers = [0, 1, 2, 3, 4, 5]

# Генератор


def squares_of_even_gen(nums):
    for i in nums:
        if i % 2 == 0:
            yield i**2


print(list(squares_of_even_gen(numbers)))

# Цикл
squares_of_even = []

for i in numbers:
    if i % 2 == 0:
        squares_of_even.append(i**2)


print(squares_of_even)
